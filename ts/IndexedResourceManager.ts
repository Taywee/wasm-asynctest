/** A simple index-based resource manager, for managing shared resources that
 * can be used from WASM.
 */
export class IndexedResourceManager<Resource> implements Iterable<Resource> {
  // The next non-empty value to set.
  #next_value: number = 0;
  #resources: Map<number, Resource> = new Map();
  #empty_slots: Set<number> = new Set();

  #popEmpty(): number | undefined {
    const next = this.#empty_slots.keys().next();
    if (next.done) {
      return undefined;
    } else {
      const index = next.value;
      this.#empty_slots.delete(index);
      return index;
    }
  }

  /** Add a resource into the manager, and return its slot.
   */
  add(resource: Resource): number {
    const index = this.#popEmpty();
    if (index === undefined) {
      const index = this.#next_value;
      this.#resources.set(index, resource);
      this.#next_value += 1;
      return index;
    } else {
      this.#resources.set(index, resource);
      return index;
    }
  }

  get(index: number): Resource {
    if (this.#resources.has(index)) {
      return this.#resources.get(index)!;
    } else {
      throw new Error('This resource does not exist');
    }
  }

  remove(index: number): Resource {
    const resource = this.get(index);
    this.#resources.delete(index);
    // If the resource is already the last resource, don't push into the empty
    // slots set.
    if (index + 1 == this.#next_value) {
      this.#next_value = index;
    } else {
      this.#empty_slots.add(index);
    }

    return resource;
  }

  [Symbol.iterator](): Iterator<Resource> {
    return this.#resources.values();
  }
}
