import { IndexedResourceManager } from "./IndexedResourceManager.js";

const response = await fetch('/asynctest.wasm');
const buffer = await response.arrayBuffer();

const module = await WebAssembly.compile(buffer);

interface Exports {
  memory: WebAssembly.Memory,
  __indirect_function_table: WebAssembly.Table,
  start_sleep_3_times(seconds: number, value: number): number,
  finish_sleep_3_times(resolved: number): number,
  poll_future(future: number, promise: number): number,
}

let instance: WebAssembly.Instance;
let exports: Exports;

const promises: IndexedResourceManager<(value: boolean) => void> = new IndexedResourceManager();

var imports = {
  env: {
    wasm_console_log(pointer: number, len: number): void {
      const array = new Uint8Array(
        exports.memory.buffer,
        pointer,
        len,
      );
      const decoder = new TextDecoder();
      console.log(decoder.decode(array));
    },

    wasm_sleep(milliseconds: number, callback: number, userdata: number): void {
      const func = exports.__indirect_function_table.get(callback);
      if (milliseconds > 0) {
        setTimeout(() => func(userdata), milliseconds);
      } else {
        func(userdata);
      }
    },

    wasm_resolve_promise(index: number): void {
      const resolve = promises.remove(index);
      resolve(false);
    },
  },
};

instance = await WebAssembly.instantiate(module, imports);
exports = instance.exports as unknown as Exports;

async function sleep_3_times(seconds: number, value: number) {
  const future = exports.start_sleep_3_times(seconds, value);

  let retval;
  let finished = false;
  while (!finished) {
    finished = await new Promise((resolve): void => {
      const promise_index = promises.add(resolve);
      const ret = exports.poll_future(future, promise_index);
      if (ret !== 0) {
        promises.remove(promise_index);
        retval = exports.finish_sleep_3_times(ret);
        resolve(true);
      }
    });
  }
  return retval;
}

const runs: Promise<void>[] = [];

runs.push((async () => {
  console.log('return 0: ', await sleep_3_times(0, 0));
})());
runs.push((async () => {
  console.log('return 1: ', await sleep_3_times(0.25, 1));
})());
runs.push((async () => {
  console.log('return 2: ', await sleep_3_times(0.40, 2));
})());
runs.push((async () => {
  console.log('return 3: ', await sleep_3_times(0.70, 3));
})());

await Promise.all(runs);

export { }
