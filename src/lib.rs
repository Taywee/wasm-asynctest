use std::{
    any::Any,
    future::Future,
    pin::Pin,
    ptr::null_mut,
    sync::Arc,
    task::{Context, RawWaker, RawWakerVTable, Waker},
    time::Duration,
};

mod ffi {
    use std::ffi::c_void;
    extern "C" {
        pub fn wasm_console_log(string: *const u8, len: usize);
        pub fn wasm_sleep(
            milliseconds: i32,
            cb: extern "C" fn(userdata: *mut c_void),
            userdata: *mut c_void,
        );
        pub fn wasm_resolve_promise(index: i32);
    }
}

mod lib {
    use std::{
        ffi::c_void,
        future::Future,
        pin::{pin, Pin},
        task::{Context, Poll, Waker},
        time::Duration,
    };

    #[derive(Debug, Default)]
    struct SleepFuture {
        finished: bool,
        waker: Option<Waker>,
    }

    impl SleepFuture {
        fn pin_set_finished(mut self: Pin<&mut Self>) {
            let finished = unsafe { &mut self.as_mut().get_unchecked_mut().finished };
            *finished = true;
            if let Some(waker) = self.pin_get_waker().take() {
                waker.wake();
            }
        }
        fn pin_get_waker(self: Pin<&mut Self>) -> &mut Option<Waker> {
            unsafe { &mut self.get_unchecked_mut().waker }
        }
    }

    impl Future for SleepFuture {
        type Output = ();

        fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
            if self.finished {
                Poll::Ready(())
            } else {
                match self.as_mut().pin_get_waker() {
                    Some(waker) => waker.clone_from(cx.waker()),
                    None => *self.as_mut().pin_get_waker() = Some(cx.waker().clone()),
                }
                Poll::Pending
            }
        }
    }

    pub async fn sleep(duration: Duration) {
        extern "C" fn callback(userdata: *mut c_void) {
            let mut sleep_future =
                unsafe { Pin::new_unchecked(&mut *(userdata as *mut SleepFuture)) };
            sleep_future.pin_set_finished();
        }

        let mut future = pin!(SleepFuture::default());
        unsafe {
            crate::ffi::wasm_sleep(duration.as_millis().try_into().unwrap(), callback, unsafe {
                future.as_mut().get_unchecked_mut() as *mut SleepFuture as *mut c_void
            });
        }
        future.await;
    }

    pub fn log<S>(message: S)
    where
        S: AsRef<str>,
    {
        let bytes = message.as_ref().as_bytes();
        unsafe {
            crate::ffi::wasm_console_log(bytes.as_ptr(), bytes.len());
        }
    }
}

async fn sleep_3_times(duration: Duration, value: i32) -> i32 {
    lib::log(&format!("sleep_3_times ({value}): sleeping 1"));
    lib::sleep(duration).await;
    lib::log(&format!("sleep_3_times ({value}): sleeping 2"));
    lib::sleep(duration).await;
    lib::log(&format!("sleep_3_times ({value}): sleeping 3"));
    lib::sleep(duration).await;
    lib::log(&format!("sleep_3_times ({value}): done"));
    value + 100000
}

#[no_mangle]
pub extern "C" fn start_sleep_3_times(
    seconds: f64,
    value: i32,
) -> *mut Pin<Box<dyn Future<Output = Box<dyn Any>>>> {
    async fn inner(duration: Duration, value: i32) -> Box<dyn Any> {
        Box::new(sleep_3_times(duration, value).await)
    }
    let future: Pin<Box<dyn Future<Output = Box<dyn Any>>>> =
        Box::pin(inner(Duration::from_secs_f64(seconds), value));
    Box::into_raw(Box::new(future))
}

#[no_mangle]
pub extern "C" fn finish_sleep_3_times(data: *mut Box<dyn Any>) -> i32 {
    let data = unsafe { Box::from_raw(data) };
    let data = data.downcast::<i32>().expect("should be a i32");
    *data
}

mod vtable {
    use std::sync::Arc;
    use std::task::RawWaker;

    pub unsafe fn clone(data: *const ()) -> RawWaker {
        Arc::increment_strong_count(data as *const i32);
        RawWaker::new(data, &crate::V_TABLE)
    }
    pub unsafe fn wake(data: *const ()) {
        let data = Arc::from_raw(data as *const i32);
        crate::ffi::wasm_resolve_promise(*data);
    }
    pub unsafe fn wake_by_ref(data: *const ()) {
        crate::ffi::wasm_resolve_promise(*(data as *const i32));
    }
    pub unsafe fn drop(data: *const ()) {
        Arc::decrement_strong_count(data as *const i32);
    }
}

const V_TABLE: RawWakerVTable = RawWakerVTable::new(
    vtable::clone,
    vtable::wake,
    vtable::wake_by_ref,
    vtable::drop,
);

#[no_mangle]
pub extern "C" fn poll_future(
    raw_future: *mut Pin<Box<dyn Future<Output = Box<dyn Any>>>>,
    promise: i32,
) -> *mut Box<dyn Any> {
    let future = unsafe { &mut *raw_future }.as_mut();
    let data = Arc::into_raw(Arc::new(promise)) as usize as *const ();
    let raw_waker = RawWaker::new(data, &V_TABLE);
    let waker = unsafe { Waker::from_raw(raw_waker) };
    let mut context = Context::from_waker(&waker);
    match future.poll(&mut context) {
        std::task::Poll::Ready(any) => {
            // Also delete the future
            unsafe {
                drop(Box::from_raw(raw_future));
            }
            Box::leak(Box::new(any))
        }
        std::task::Poll::Pending => null_mut(),
    }
}
